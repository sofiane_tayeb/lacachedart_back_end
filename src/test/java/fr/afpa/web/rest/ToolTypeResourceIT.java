package fr.afpa.web.rest;

import fr.afpa.LacachedartApp;
import fr.afpa.domain.ToolType;
import fr.afpa.repository.ToolTypeRepository;
import fr.afpa.service.ToolTypeService;
import fr.afpa.service.dto.ToolTypeDTO;
import fr.afpa.service.mapper.ToolTypeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ToolTypeResource} REST controller.
 */
@SpringBootTest(classes = LacachedartApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ToolTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ToolTypeRepository toolTypeRepository;

    @Autowired
    private ToolTypeMapper toolTypeMapper;

    @Autowired
    private ToolTypeService toolTypeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restToolTypeMockMvc;

    private ToolType toolType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ToolType createEntity(EntityManager em) {
        ToolType toolType = new ToolType()
            .name(DEFAULT_NAME);
        return toolType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ToolType createUpdatedEntity(EntityManager em) {
        ToolType toolType = new ToolType()
            .name(UPDATED_NAME);
        return toolType;
    }

    @BeforeEach
    public void initTest() {
        toolType = createEntity(em);
    }

    @Test
    @Transactional
    public void createToolType() throws Exception {
        int databaseSizeBeforeCreate = toolTypeRepository.findAll().size();
        // Create the ToolType
        ToolTypeDTO toolTypeDTO = toolTypeMapper.toDto(toolType);
        restToolTypeMockMvc.perform(post("/api/tool-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(toolTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the ToolType in the database
        List<ToolType> toolTypeList = toolTypeRepository.findAll();
        assertThat(toolTypeList).hasSize(databaseSizeBeforeCreate + 1);
        ToolType testToolType = toolTypeList.get(toolTypeList.size() - 1);
        assertThat(testToolType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createToolTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = toolTypeRepository.findAll().size();

        // Create the ToolType with an existing ID
        toolType.setId(1L);
        ToolTypeDTO toolTypeDTO = toolTypeMapper.toDto(toolType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restToolTypeMockMvc.perform(post("/api/tool-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(toolTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ToolType in the database
        List<ToolType> toolTypeList = toolTypeRepository.findAll();
        assertThat(toolTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllToolTypes() throws Exception {
        // Initialize the database
        toolTypeRepository.saveAndFlush(toolType);

        // Get all the toolTypeList
        restToolTypeMockMvc.perform(get("/api/tool-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(toolType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getToolType() throws Exception {
        // Initialize the database
        toolTypeRepository.saveAndFlush(toolType);

        // Get the toolType
        restToolTypeMockMvc.perform(get("/api/tool-types/{id}", toolType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(toolType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingToolType() throws Exception {
        // Get the toolType
        restToolTypeMockMvc.perform(get("/api/tool-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateToolType() throws Exception {
        // Initialize the database
        toolTypeRepository.saveAndFlush(toolType);

        int databaseSizeBeforeUpdate = toolTypeRepository.findAll().size();

        // Update the toolType
        ToolType updatedToolType = toolTypeRepository.findById(toolType.getId()).get();
        // Disconnect from session so that the updates on updatedToolType are not directly saved in db
        em.detach(updatedToolType);
        updatedToolType
            .name(UPDATED_NAME);
        ToolTypeDTO toolTypeDTO = toolTypeMapper.toDto(updatedToolType);

        restToolTypeMockMvc.perform(put("/api/tool-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(toolTypeDTO)))
            .andExpect(status().isOk());

        // Validate the ToolType in the database
        List<ToolType> toolTypeList = toolTypeRepository.findAll();
        assertThat(toolTypeList).hasSize(databaseSizeBeforeUpdate);
        ToolType testToolType = toolTypeList.get(toolTypeList.size() - 1);
        assertThat(testToolType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingToolType() throws Exception {
        int databaseSizeBeforeUpdate = toolTypeRepository.findAll().size();

        // Create the ToolType
        ToolTypeDTO toolTypeDTO = toolTypeMapper.toDto(toolType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restToolTypeMockMvc.perform(put("/api/tool-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(toolTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ToolType in the database
        List<ToolType> toolTypeList = toolTypeRepository.findAll();
        assertThat(toolTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteToolType() throws Exception {
        // Initialize the database
        toolTypeRepository.saveAndFlush(toolType);

        int databaseSizeBeforeDelete = toolTypeRepository.findAll().size();

        // Delete the toolType
        restToolTypeMockMvc.perform(delete("/api/tool-types/{id}", toolType.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ToolType> toolTypeList = toolTypeRepository.findAll();
        assertThat(toolTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
