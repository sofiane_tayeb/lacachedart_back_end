package fr.afpa.web.rest;

import fr.afpa.LacachedartApp;
import fr.afpa.domain.UserOtherInfo;
import fr.afpa.repository.UserOtherInfoRepository;
import fr.afpa.service.UserOtherInfoService;
import fr.afpa.service.dto.UserOtherInfoDTO;
import fr.afpa.service.mapper.UserOtherInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static fr.afpa.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserOtherInfoResource} REST controller.
 */
@SpringBootTest(classes = LacachedartApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UserOtherInfoResourceIT {

    private static final ZonedDateTime DEFAULT_SUBSCRIPTION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SUBSCRIPTION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_POINTS = 1;
    private static final Integer UPDATED_POINTS = 2;

    private static final Boolean DEFAULT_BANNED = false;
    private static final Boolean UPDATED_BANNED = true;

    @Autowired
    private UserOtherInfoRepository userOtherInfoRepository;

    @Autowired
    private UserOtherInfoMapper userOtherInfoMapper;

    @Autowired
    private UserOtherInfoService userOtherInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserOtherInfoMockMvc;

    private UserOtherInfo userOtherInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserOtherInfo createEntity(EntityManager em) {
        UserOtherInfo userOtherInfo = new UserOtherInfo()
            .subscriptionDate(DEFAULT_SUBSCRIPTION_DATE)
            .points(DEFAULT_POINTS)
            .banned(DEFAULT_BANNED);
        return userOtherInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserOtherInfo createUpdatedEntity(EntityManager em) {
        UserOtherInfo userOtherInfo = new UserOtherInfo()
            .subscriptionDate(UPDATED_SUBSCRIPTION_DATE)
            .points(UPDATED_POINTS)
            .banned(UPDATED_BANNED);
        return userOtherInfo;
    }

    @BeforeEach
    public void initTest() {
        userOtherInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserOtherInfo() throws Exception {
        int databaseSizeBeforeCreate = userOtherInfoRepository.findAll().size();
        // Create the UserOtherInfo
        UserOtherInfoDTO userOtherInfoDTO = userOtherInfoMapper.toDto(userOtherInfo);
        restUserOtherInfoMockMvc.perform(post("/api/user-other-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userOtherInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the UserOtherInfo in the database
        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeCreate + 1);
        UserOtherInfo testUserOtherInfo = userOtherInfoList.get(userOtherInfoList.size() - 1);
        assertThat(testUserOtherInfo.getSubscriptionDate()).isEqualTo(DEFAULT_SUBSCRIPTION_DATE);
        assertThat(testUserOtherInfo.getPoints()).isEqualTo(DEFAULT_POINTS);
        assertThat(testUserOtherInfo.isBanned()).isEqualTo(DEFAULT_BANNED);
    }

    @Test
    @Transactional
    public void createUserOtherInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userOtherInfoRepository.findAll().size();

        // Create the UserOtherInfo with an existing ID
        userOtherInfo.setId(1L);
        UserOtherInfoDTO userOtherInfoDTO = userOtherInfoMapper.toDto(userOtherInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserOtherInfoMockMvc.perform(post("/api/user-other-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userOtherInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserOtherInfo in the database
        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkBannedIsRequired() throws Exception {
        int databaseSizeBeforeTest = userOtherInfoRepository.findAll().size();
        // set the field null
        userOtherInfo.setBanned(null);

        // Create the UserOtherInfo, which fails.
        UserOtherInfoDTO userOtherInfoDTO = userOtherInfoMapper.toDto(userOtherInfo);


        restUserOtherInfoMockMvc.perform(post("/api/user-other-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userOtherInfoDTO)))
            .andExpect(status().isBadRequest());

        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserOtherInfos() throws Exception {
        // Initialize the database
        userOtherInfoRepository.saveAndFlush(userOtherInfo);

        // Get all the userOtherInfoList
        restUserOtherInfoMockMvc.perform(get("/api/user-other-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userOtherInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].subscriptionDate").value(hasItem(sameInstant(DEFAULT_SUBSCRIPTION_DATE))))
            .andExpect(jsonPath("$.[*].points").value(hasItem(DEFAULT_POINTS)))
            .andExpect(jsonPath("$.[*].banned").value(hasItem(DEFAULT_BANNED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getUserOtherInfo() throws Exception {
        // Initialize the database
        userOtherInfoRepository.saveAndFlush(userOtherInfo);

        // Get the userOtherInfo
        restUserOtherInfoMockMvc.perform(get("/api/user-other-infos/{id}", userOtherInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userOtherInfo.getId().intValue()))
            .andExpect(jsonPath("$.subscriptionDate").value(sameInstant(DEFAULT_SUBSCRIPTION_DATE)))
            .andExpect(jsonPath("$.points").value(DEFAULT_POINTS))
            .andExpect(jsonPath("$.banned").value(DEFAULT_BANNED.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserOtherInfo() throws Exception {
        // Get the userOtherInfo
        restUserOtherInfoMockMvc.perform(get("/api/user-other-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserOtherInfo() throws Exception {
        // Initialize the database
        userOtherInfoRepository.saveAndFlush(userOtherInfo);

        int databaseSizeBeforeUpdate = userOtherInfoRepository.findAll().size();

        // Update the userOtherInfo
        UserOtherInfo updatedUserOtherInfo = userOtherInfoRepository.findById(userOtherInfo.getId()).get();
        // Disconnect from session so that the updates on updatedUserOtherInfo are not directly saved in db
        em.detach(updatedUserOtherInfo);
        updatedUserOtherInfo
            .subscriptionDate(UPDATED_SUBSCRIPTION_DATE)
            .points(UPDATED_POINTS)
            .banned(UPDATED_BANNED);
        UserOtherInfoDTO userOtherInfoDTO = userOtherInfoMapper.toDto(updatedUserOtherInfo);

        restUserOtherInfoMockMvc.perform(put("/api/user-other-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userOtherInfoDTO)))
            .andExpect(status().isOk());

        // Validate the UserOtherInfo in the database
        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeUpdate);
        UserOtherInfo testUserOtherInfo = userOtherInfoList.get(userOtherInfoList.size() - 1);
        assertThat(testUserOtherInfo.getSubscriptionDate()).isEqualTo(UPDATED_SUBSCRIPTION_DATE);
        assertThat(testUserOtherInfo.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testUserOtherInfo.isBanned()).isEqualTo(UPDATED_BANNED);
    }

    @Test
    @Transactional
    public void updateNonExistingUserOtherInfo() throws Exception {
        int databaseSizeBeforeUpdate = userOtherInfoRepository.findAll().size();

        // Create the UserOtherInfo
        UserOtherInfoDTO userOtherInfoDTO = userOtherInfoMapper.toDto(userOtherInfo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserOtherInfoMockMvc.perform(put("/api/user-other-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userOtherInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserOtherInfo in the database
        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserOtherInfo() throws Exception {
        // Initialize the database
        userOtherInfoRepository.saveAndFlush(userOtherInfo);

        int databaseSizeBeforeDelete = userOtherInfoRepository.findAll().size();

        // Delete the userOtherInfo
        restUserOtherInfoMockMvc.perform(delete("/api/user-other-infos/{id}", userOtherInfo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserOtherInfo> userOtherInfoList = userOtherInfoRepository.findAll();
        assertThat(userOtherInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
