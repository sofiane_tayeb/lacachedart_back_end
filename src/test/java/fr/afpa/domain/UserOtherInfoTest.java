package fr.afpa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.afpa.web.rest.TestUtil;

public class UserOtherInfoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserOtherInfo.class);
        UserOtherInfo userOtherInfo1 = new UserOtherInfo();
        userOtherInfo1.setId(1L);
        UserOtherInfo userOtherInfo2 = new UserOtherInfo();
        userOtherInfo2.setId(userOtherInfo1.getId());
        assertThat(userOtherInfo1).isEqualTo(userOtherInfo2);
        userOtherInfo2.setId(2L);
        assertThat(userOtherInfo1).isNotEqualTo(userOtherInfo2);
        userOtherInfo1.setId(null);
        assertThat(userOtherInfo1).isNotEqualTo(userOtherInfo2);
    }
}
