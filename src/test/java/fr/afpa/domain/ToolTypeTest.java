package fr.afpa.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.afpa.web.rest.TestUtil;

public class ToolTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ToolType.class);
        ToolType toolType1 = new ToolType();
        toolType1.setId(1L);
        ToolType toolType2 = new ToolType();
        toolType2.setId(toolType1.getId());
        assertThat(toolType1).isEqualTo(toolType2);
        toolType2.setId(2L);
        assertThat(toolType1).isNotEqualTo(toolType2);
        toolType1.setId(null);
        assertThat(toolType1).isNotEqualTo(toolType2);
    }
}
