package fr.afpa.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserOtherInfoMapperTest {

    private UserOtherInfoMapper userOtherInfoMapper;

    @BeforeEach
    public void setUp() {
        userOtherInfoMapper = new UserOtherInfoMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userOtherInfoMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userOtherInfoMapper.fromId(null)).isNull();
    }
}
