package fr.afpa.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ToolTypeMapperTest {

    private ToolTypeMapper toolTypeMapper;

    @BeforeEach
    public void setUp() {
        toolTypeMapper = new ToolTypeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(toolTypeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(toolTypeMapper.fromId(null)).isNull();
    }
}
