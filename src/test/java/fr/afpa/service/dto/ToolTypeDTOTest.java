package fr.afpa.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import fr.afpa.web.rest.TestUtil;

public class ToolTypeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ToolTypeDTO.class);
        ToolTypeDTO toolTypeDTO1 = new ToolTypeDTO();
        toolTypeDTO1.setId(1L);
        ToolTypeDTO toolTypeDTO2 = new ToolTypeDTO();
        assertThat(toolTypeDTO1).isNotEqualTo(toolTypeDTO2);
        toolTypeDTO2.setId(toolTypeDTO1.getId());
        assertThat(toolTypeDTO1).isEqualTo(toolTypeDTO2);
        toolTypeDTO2.setId(2L);
        assertThat(toolTypeDTO1).isNotEqualTo(toolTypeDTO2);
        toolTypeDTO1.setId(null);
        assertThat(toolTypeDTO1).isNotEqualTo(toolTypeDTO2);
    }
}
