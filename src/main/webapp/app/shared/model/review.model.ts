import { IPhoto } from 'app/shared/model/photo.model';

export interface IReview {
  id?: number;
  content?: string;
  note?: number;
  active?: boolean;
  userOtherInfoId?: number;
  productId?: number;
  photos?: IPhoto[];
}

export const defaultValue: Readonly<IReview> = {
  active: false,
};
