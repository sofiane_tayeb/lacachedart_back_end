import { IProduct } from 'app/shared/model/product.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IToolType {
  id?: number;
  name?: string;
  products?: IProduct[];
  categories?: ICategory[];
}

export const defaultValue: Readonly<IToolType> = {};
