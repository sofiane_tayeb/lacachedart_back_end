export interface IPhoto {
  id?: number;
  link?: string;
  active?: boolean;
  userOtherInfoId?: number;
  productId?: number;
  reviewId?: number;
}

export const defaultValue: Readonly<IPhoto> = {
  active: false,
};
