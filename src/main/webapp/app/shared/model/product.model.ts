import { Moment } from 'moment';
import { ICategory } from 'app/shared/model/category.model';
import { IStore } from 'app/shared/model/store.model';
import { IPhoto } from 'app/shared/model/photo.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string;
  manufacturerPrice?: number;
  creationDate?: string;
  reference?: string;
  active?: boolean;
  userOtherInfoId?: number;
  categoryId?: number;
  brandId?: number;
  toolTypeId?: number;
  categories?: ICategory[];
  stores?: IStore[];
  photos?: IPhoto[];
}

export const defaultValue: Readonly<IProduct> = {
  active: false,
};
