import { IProduct } from 'app/shared/model/product.model';

export interface IStore {
  id?: number;
  name?: string;
  streetNumber?: string;
  streetName?: string;
  complement?: string;
  city?: string;
  postalCode?: string;
  country?: string;
  url?: string;
  gps?: string;
  products?: IProduct[];
}

export const defaultValue: Readonly<IStore> = {};
