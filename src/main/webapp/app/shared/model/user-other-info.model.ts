import { Moment } from 'moment';

export interface IUserOtherInfo {
  id?: number;
  subscriptionDate?: string;
  points?: number;
  banned?: boolean;
  userId?: number;
}

export const defaultValue: Readonly<IUserOtherInfo> = {
  banned: false,
};
