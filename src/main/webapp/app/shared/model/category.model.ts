import { IProduct } from 'app/shared/model/product.model';
import { IToolType } from 'app/shared/model/tool-type.model';

export interface ICategory {
  id?: number;
  name?: string;
  parentId?: number;
  products?: IProduct[];
  toolTypes?: IToolType[];
}

export const defaultValue: Readonly<ICategory> = {};
