import { IProduct } from 'app/shared/model/product.model';

export interface IBrand {
  id?: number;
  name?: string;
  products?: IProduct[];
}

export const defaultValue: Readonly<IBrand> = {};
