import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductDetail = (props: IProductDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="lacachedartApp.product.detail.title">Product</Translate> [<b>{productEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="lacachedartApp.product.name">Name</Translate>
            </span>
          </dt>
          <dd>{productEntity.name}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="lacachedartApp.product.description">Description</Translate>
            </span>
          </dt>
          <dd>{productEntity.description}</dd>
          <dt>
            <span id="manufacturerPrice">
              <Translate contentKey="lacachedartApp.product.manufacturerPrice">Manufacturer Price</Translate>
            </span>
          </dt>
          <dd>{productEntity.manufacturerPrice}</dd>
          <dt>
            <span id="creationDate">
              <Translate contentKey="lacachedartApp.product.creationDate">Creation Date</Translate>
            </span>
          </dt>
          <dd>
            {productEntity.creationDate ? <TextFormat value={productEntity.creationDate} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="reference">
              <Translate contentKey="lacachedartApp.product.reference">Reference</Translate>
            </span>
          </dt>
          <dd>{productEntity.reference}</dd>
          <dt>
            <span id="active">
              <Translate contentKey="lacachedartApp.product.active">Active</Translate>
            </span>
          </dt>
          <dd>{productEntity.active ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.product.userOtherInfo">User Other Info</Translate>
          </dt>
          <dd>{productEntity.userOtherInfoId ? productEntity.userOtherInfoId : ''}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.product.category">Category</Translate>
          </dt>
          <dd>{productEntity.categoryId ? productEntity.categoryId : ''}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.product.brand">Brand</Translate>
          </dt>
          <dd>{productEntity.brandId ? productEntity.brandId : ''}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.product.toolType">Tool Type</Translate>
          </dt>
          <dd>{productEntity.toolTypeId ? productEntity.toolTypeId : ''}</dd>
        </dl>
        <Button tag={Link} to="/product" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product/${productEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ product }: IRootState) => ({
  productEntity: product.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
