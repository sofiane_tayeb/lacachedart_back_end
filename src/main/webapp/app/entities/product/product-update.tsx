import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUserOtherInfo } from 'app/shared/model/user-other-info.model';
import { getEntities as getUserOtherInfos } from 'app/entities/user-other-info/user-other-info.reducer';
import { ICategory } from 'app/shared/model/category.model';
import { getEntities as getCategories } from 'app/entities/category/category.reducer';
import { IBrand } from 'app/shared/model/brand.model';
import { getEntities as getBrands } from 'app/entities/brand/brand.reducer';
import { IToolType } from 'app/shared/model/tool-type.model';
import { getEntities as getToolTypes } from 'app/entities/tool-type/tool-type.reducer';
import { IStore } from 'app/shared/model/store.model';
import { getEntities as getStores } from 'app/entities/store/store.reducer';
import { getEntity, updateEntity, createEntity, reset } from './product.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProductUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductUpdate = (props: IProductUpdateProps) => {
  const [userOtherInfoId, setUserOtherInfoId] = useState('0');
  const [categoryId, setCategoryId] = useState('0');
  const [categoriesId, setCategoriesId] = useState('0');
  const [brandId, setBrandId] = useState('0');
  const [toolTypeId, setToolTypeId] = useState('0');
  const [storesId, setStoresId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { productEntity, userOtherInfos, categories, brands, toolTypes, stores, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/product' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUserOtherInfos();
    props.getCategories();
    props.getBrands();
    props.getToolTypes();
    props.getStores();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationDate = convertDateTimeToServer(values.creationDate);

    if (errors.length === 0) {
      const entity = {
        ...productEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lacachedartApp.product.home.createOrEditLabel">
            <Translate contentKey="lacachedartApp.product.home.createOrEditLabel">Create or edit a Product</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : productEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="product-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="product-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="product-name">
                  <Translate contentKey="lacachedartApp.product.name">Name</Translate>
                </Label>
                <AvField
                  id="product-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="descriptionLabel" for="product-description">
                  <Translate contentKey="lacachedartApp.product.description">Description</Translate>
                </Label>
                <AvField
                  id="product-description"
                  type="text"
                  name="description"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="manufacturerPriceLabel" for="product-manufacturerPrice">
                  <Translate contentKey="lacachedartApp.product.manufacturerPrice">Manufacturer Price</Translate>
                </Label>
                <AvField id="product-manufacturerPrice" type="string" className="form-control" name="manufacturerPrice" />
              </AvGroup>
              <AvGroup>
                <Label id="creationDateLabel" for="product-creationDate">
                  <Translate contentKey="lacachedartApp.product.creationDate">Creation Date</Translate>
                </Label>
                <AvInput
                  id="product-creationDate"
                  type="datetime-local"
                  className="form-control"
                  name="creationDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.productEntity.creationDate)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="referenceLabel" for="product-reference">
                  <Translate contentKey="lacachedartApp.product.reference">Reference</Translate>
                </Label>
                <AvField
                  id="product-reference"
                  type="text"
                  name="reference"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="activeLabel">
                  <AvInput id="product-active" type="checkbox" className="form-check-input" name="active" />
                  <Translate contentKey="lacachedartApp.product.active">Active</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="product-userOtherInfo">
                  <Translate contentKey="lacachedartApp.product.userOtherInfo">User Other Info</Translate>
                </Label>
                <AvInput id="product-userOtherInfo" type="select" className="form-control" name="userOtherInfoId" required>
                  {userOtherInfos
                    ? userOtherInfos.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="product-category">
                  <Translate contentKey="lacachedartApp.product.category">Category</Translate>
                </Label>
                <AvInput id="product-category" type="select" className="form-control" name="categoryId" required>
                  {categories
                    ? categories.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="product-brand">
                  <Translate contentKey="lacachedartApp.product.brand">Brand</Translate>
                </Label>
                <AvInput id="product-brand" type="select" className="form-control" name="brandId" required>
                  {brands
                    ? brands.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="product-toolType">
                  <Translate contentKey="lacachedartApp.product.toolType">Tool Type</Translate>
                </Label>
                <AvInput id="product-toolType" type="select" className="form-control" name="toolTypeId" required>
                  {toolTypes
                    ? toolTypes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/product" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  userOtherInfos: storeState.userOtherInfo.entities,
  categories: storeState.category.entities,
  brands: storeState.brand.entities,
  toolTypes: storeState.toolType.entities,
  stores: storeState.store.entities,
  productEntity: storeState.product.entity,
  loading: storeState.product.loading,
  updating: storeState.product.updating,
  updateSuccess: storeState.product.updateSuccess,
});

const mapDispatchToProps = {
  getUserOtherInfos,
  getCategories,
  getBrands,
  getToolTypes,
  getStores,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductUpdate);
