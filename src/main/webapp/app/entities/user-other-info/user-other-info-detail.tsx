import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-other-info.reducer';
import { IUserOtherInfo } from 'app/shared/model/user-other-info.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserOtherInfoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserOtherInfoDetail = (props: IUserOtherInfoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { userOtherInfoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="lacachedartApp.userOtherInfo.detail.title">UserOtherInfo</Translate> [<b>{userOtherInfoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="subscriptionDate">
              <Translate contentKey="lacachedartApp.userOtherInfo.subscriptionDate">Subscription Date</Translate>
            </span>
          </dt>
          <dd>
            {userOtherInfoEntity.subscriptionDate ? (
              <TextFormat value={userOtherInfoEntity.subscriptionDate} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="points">
              <Translate contentKey="lacachedartApp.userOtherInfo.points">Points</Translate>
            </span>
          </dt>
          <dd>{userOtherInfoEntity.points}</dd>
          <dt>
            <span id="banned">
              <Translate contentKey="lacachedartApp.userOtherInfo.banned">Banned</Translate>
            </span>
          </dt>
          <dd>{userOtherInfoEntity.banned ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.userOtherInfo.user">User</Translate>
          </dt>
          <dd>{userOtherInfoEntity.userId ? userOtherInfoEntity.userId : ''}</dd>
        </dl>
        <Button tag={Link} to="/user-other-info" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/user-other-info/${userOtherInfoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ userOtherInfo }: IRootState) => ({
  userOtherInfoEntity: userOtherInfo.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserOtherInfoDetail);
