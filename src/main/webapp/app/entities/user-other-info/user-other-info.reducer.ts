import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUserOtherInfo, defaultValue } from 'app/shared/model/user-other-info.model';

export const ACTION_TYPES = {
  FETCH_USEROTHERINFO_LIST: 'userOtherInfo/FETCH_USEROTHERINFO_LIST',
  FETCH_USEROTHERINFO: 'userOtherInfo/FETCH_USEROTHERINFO',
  CREATE_USEROTHERINFO: 'userOtherInfo/CREATE_USEROTHERINFO',
  UPDATE_USEROTHERINFO: 'userOtherInfo/UPDATE_USEROTHERINFO',
  DELETE_USEROTHERINFO: 'userOtherInfo/DELETE_USEROTHERINFO',
  RESET: 'userOtherInfo/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserOtherInfo>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type UserOtherInfoState = Readonly<typeof initialState>;

// Reducer

export default (state: UserOtherInfoState = initialState, action): UserOtherInfoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USEROTHERINFO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USEROTHERINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_USEROTHERINFO):
    case REQUEST(ACTION_TYPES.UPDATE_USEROTHERINFO):
    case REQUEST(ACTION_TYPES.DELETE_USEROTHERINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_USEROTHERINFO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USEROTHERINFO):
    case FAILURE(ACTION_TYPES.CREATE_USEROTHERINFO):
    case FAILURE(ACTION_TYPES.UPDATE_USEROTHERINFO):
    case FAILURE(ACTION_TYPES.DELETE_USEROTHERINFO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_USEROTHERINFO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_USEROTHERINFO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_USEROTHERINFO):
    case SUCCESS(ACTION_TYPES.UPDATE_USEROTHERINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_USEROTHERINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/user-other-infos';

// Actions

export const getEntities: ICrudGetAllAction<IUserOtherInfo> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_USEROTHERINFO_LIST,
    payload: axios.get<IUserOtherInfo>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IUserOtherInfo> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USEROTHERINFO,
    payload: axios.get<IUserOtherInfo>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IUserOtherInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USEROTHERINFO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserOtherInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USEROTHERINFO,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserOtherInfo> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USEROTHERINFO,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
