import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-other-info.reducer';
import { IUserOtherInfo } from 'app/shared/model/user-other-info.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserOtherInfoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserOtherInfoUpdate = (props: IUserOtherInfoUpdateProps) => {
  const [userId, setUserId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { userOtherInfoEntity, users, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/user-other-info' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getUsers();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.subscriptionDate = convertDateTimeToServer(values.subscriptionDate);

    if (errors.length === 0) {
      const entity = {
        ...userOtherInfoEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lacachedartApp.userOtherInfo.home.createOrEditLabel">
            <Translate contentKey="lacachedartApp.userOtherInfo.home.createOrEditLabel">Create or edit a UserOtherInfo</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : userOtherInfoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="user-other-info-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="user-other-info-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="subscriptionDateLabel" for="user-other-info-subscriptionDate">
                  <Translate contentKey="lacachedartApp.userOtherInfo.subscriptionDate">Subscription Date</Translate>
                </Label>
                <AvInput
                  id="user-other-info-subscriptionDate"
                  type="datetime-local"
                  className="form-control"
                  name="subscriptionDate"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.userOtherInfoEntity.subscriptionDate)}
                />
              </AvGroup>
              <AvGroup>
                <Label id="pointsLabel" for="user-other-info-points">
                  <Translate contentKey="lacachedartApp.userOtherInfo.points">Points</Translate>
                </Label>
                <AvField id="user-other-info-points" type="string" className="form-control" name="points" />
              </AvGroup>
              <AvGroup check>
                <Label id="bannedLabel">
                  <AvInput id="user-other-info-banned" type="checkbox" className="form-check-input" name="banned" />
                  <Translate contentKey="lacachedartApp.userOtherInfo.banned">Banned</Translate>
                </Label>
              </AvGroup>
              <AvGroup>
                <Label for="user-other-info-user">
                  <Translate contentKey="lacachedartApp.userOtherInfo.user">User</Translate>
                </Label>
                <AvInput id="user-other-info-user" type="select" className="form-control" name="userId">
                  <option value="" key="0" />
                  {users
                    ? users.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/user-other-info" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  userOtherInfoEntity: storeState.userOtherInfo.entity,
  loading: storeState.userOtherInfo.loading,
  updating: storeState.userOtherInfo.updating,
  updateSuccess: storeState.userOtherInfo.updateSuccess,
});

const mapDispatchToProps = {
  getUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserOtherInfoUpdate);
