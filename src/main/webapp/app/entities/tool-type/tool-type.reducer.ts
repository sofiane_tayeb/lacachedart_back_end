import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IToolType, defaultValue } from 'app/shared/model/tool-type.model';

export const ACTION_TYPES = {
  FETCH_TOOLTYPE_LIST: 'toolType/FETCH_TOOLTYPE_LIST',
  FETCH_TOOLTYPE: 'toolType/FETCH_TOOLTYPE',
  CREATE_TOOLTYPE: 'toolType/CREATE_TOOLTYPE',
  UPDATE_TOOLTYPE: 'toolType/UPDATE_TOOLTYPE',
  DELETE_TOOLTYPE: 'toolType/DELETE_TOOLTYPE',
  RESET: 'toolType/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IToolType>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ToolTypeState = Readonly<typeof initialState>;

// Reducer

export default (state: ToolTypeState = initialState, action): ToolTypeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TOOLTYPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TOOLTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_TOOLTYPE):
    case REQUEST(ACTION_TYPES.UPDATE_TOOLTYPE):
    case REQUEST(ACTION_TYPES.DELETE_TOOLTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_TOOLTYPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TOOLTYPE):
    case FAILURE(ACTION_TYPES.CREATE_TOOLTYPE):
    case FAILURE(ACTION_TYPES.UPDATE_TOOLTYPE):
    case FAILURE(ACTION_TYPES.DELETE_TOOLTYPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOOLTYPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_TOOLTYPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_TOOLTYPE):
    case SUCCESS(ACTION_TYPES.UPDATE_TOOLTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_TOOLTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/tool-types';

// Actions

export const getEntities: ICrudGetAllAction<IToolType> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TOOLTYPE_LIST,
    payload: axios.get<IToolType>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IToolType> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TOOLTYPE,
    payload: axios.get<IToolType>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IToolType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TOOLTYPE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IToolType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TOOLTYPE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IToolType> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TOOLTYPE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
