import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ToolType from './tool-type';
import ToolTypeDetail from './tool-type-detail';
import ToolTypeUpdate from './tool-type-update';
import ToolTypeDeleteDialog from './tool-type-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ToolTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ToolTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ToolTypeDetail} />
      <ErrorBoundaryRoute path={match.url} component={ToolType} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ToolTypeDeleteDialog} />
  </>
);

export default Routes;
