import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './store.reducer';
import { IStore } from 'app/shared/model/store.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IStoreDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StoreDetail = (props: IStoreDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { storeEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="lacachedartApp.store.detail.title">Store</Translate> [<b>{storeEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="lacachedartApp.store.name">Name</Translate>
            </span>
          </dt>
          <dd>{storeEntity.name}</dd>
          <dt>
            <span id="streetNumber">
              <Translate contentKey="lacachedartApp.store.streetNumber">Street Number</Translate>
            </span>
          </dt>
          <dd>{storeEntity.streetNumber}</dd>
          <dt>
            <span id="streetName">
              <Translate contentKey="lacachedartApp.store.streetName">Street Name</Translate>
            </span>
          </dt>
          <dd>{storeEntity.streetName}</dd>
          <dt>
            <span id="complement">
              <Translate contentKey="lacachedartApp.store.complement">Complement</Translate>
            </span>
          </dt>
          <dd>{storeEntity.complement}</dd>
          <dt>
            <span id="city">
              <Translate contentKey="lacachedartApp.store.city">City</Translate>
            </span>
          </dt>
          <dd>{storeEntity.city}</dd>
          <dt>
            <span id="postalCode">
              <Translate contentKey="lacachedartApp.store.postalCode">Postal Code</Translate>
            </span>
          </dt>
          <dd>{storeEntity.postalCode}</dd>
          <dt>
            <span id="country">
              <Translate contentKey="lacachedartApp.store.country">Country</Translate>
            </span>
          </dt>
          <dd>{storeEntity.country}</dd>
          <dt>
            <span id="url">
              <Translate contentKey="lacachedartApp.store.url">Url</Translate>
            </span>
          </dt>
          <dd>{storeEntity.url}</dd>
          <dt>
            <span id="gps">
              <Translate contentKey="lacachedartApp.store.gps">Gps</Translate>
            </span>
          </dt>
          <dd>{storeEntity.gps}</dd>
          <dt>
            <Translate contentKey="lacachedartApp.store.products">Products</Translate>
          </dt>
          <dd>
            {storeEntity.products
              ? storeEntity.products.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {storeEntity.products && i === storeEntity.products.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/store" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/store/${storeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ store }: IRootState) => ({
  storeEntity: store.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StoreDetail);
