import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { getEntity, updateEntity, createEntity, reset } from './store.reducer';
import { IStore } from 'app/shared/model/store.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IStoreUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StoreUpdate = (props: IStoreUpdateProps) => {
  const [idsproducts, setIdsproducts] = useState([]);
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { storeEntity, products, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/store');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...storeEntity,
        ...values,
        products: mapIdList(values.products),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="lacachedartApp.store.home.createOrEditLabel">
            <Translate contentKey="lacachedartApp.store.home.createOrEditLabel">Create or edit a Store</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : storeEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="store-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="store-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="store-name">
                  <Translate contentKey="lacachedartApp.store.name">Name</Translate>
                </Label>
                <AvField id="store-name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label id="streetNumberLabel" for="store-streetNumber">
                  <Translate contentKey="lacachedartApp.store.streetNumber">Street Number</Translate>
                </Label>
                <AvField id="store-streetNumber" type="text" name="streetNumber" />
              </AvGroup>
              <AvGroup>
                <Label id="streetNameLabel" for="store-streetName">
                  <Translate contentKey="lacachedartApp.store.streetName">Street Name</Translate>
                </Label>
                <AvField id="store-streetName" type="text" name="streetName" />
              </AvGroup>
              <AvGroup>
                <Label id="complementLabel" for="store-complement">
                  <Translate contentKey="lacachedartApp.store.complement">Complement</Translate>
                </Label>
                <AvField id="store-complement" type="text" name="complement" />
              </AvGroup>
              <AvGroup>
                <Label id="cityLabel" for="store-city">
                  <Translate contentKey="lacachedartApp.store.city">City</Translate>
                </Label>
                <AvField id="store-city" type="text" name="city" />
              </AvGroup>
              <AvGroup>
                <Label id="postalCodeLabel" for="store-postalCode">
                  <Translate contentKey="lacachedartApp.store.postalCode">Postal Code</Translate>
                </Label>
                <AvField id="store-postalCode" type="text" name="postalCode" />
              </AvGroup>
              <AvGroup>
                <Label id="countryLabel" for="store-country">
                  <Translate contentKey="lacachedartApp.store.country">Country</Translate>
                </Label>
                <AvField id="store-country" type="text" name="country" />
              </AvGroup>
              <AvGroup>
                <Label id="urlLabel" for="store-url">
                  <Translate contentKey="lacachedartApp.store.url">Url</Translate>
                </Label>
                <AvField id="store-url" type="text" name="url" />
              </AvGroup>
              <AvGroup>
                <Label id="gpsLabel" for="store-gps">
                  <Translate contentKey="lacachedartApp.store.gps">Gps</Translate>
                </Label>
                <AvField id="store-gps" type="text" name="gps" />
              </AvGroup>
              <AvGroup>
                <Label for="store-products">
                  <Translate contentKey="lacachedartApp.store.products">Products</Translate>
                </Label>
                <AvInput
                  id="store-products"
                  type="select"
                  multiple
                  className="form-control"
                  name="products"
                  value={storeEntity.products && storeEntity.products.map(e => e.id)}
                >
                  <option value="" key="0" />
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/store" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  products: storeState.product.entities,
  storeEntity: storeState.store.entity,
  loading: storeState.store.loading,
  updating: storeState.store.updating,
  updateSuccess: storeState.store.updateSuccess,
});

const mapDispatchToProps = {
  getProducts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StoreUpdate);
