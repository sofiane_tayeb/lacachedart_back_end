import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './store.reducer';
import { IStore } from 'app/shared/model/store.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IStoreProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Store = (props: IStoreProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { storeList, match, loading } = props;
  return (
    <div>
      <h2 id="store-heading">
        <Translate contentKey="lacachedartApp.store.home.title">Stores</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="lacachedartApp.store.home.createLabel">Create new Store</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {storeList && storeList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.name">Name</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.streetNumber">Street Number</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.streetName">Street Name</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.complement">Complement</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.city">City</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.postalCode">Postal Code</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.country">Country</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.url">Url</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.gps">Gps</Translate>
                </th>
                <th>
                  <Translate contentKey="lacachedartApp.store.products">Products</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {storeList.map((store, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${store.id}`} color="link" size="sm">
                      {store.id}
                    </Button>
                  </td>
                  <td>{store.name}</td>
                  <td>{store.streetNumber}</td>
                  <td>{store.streetName}</td>
                  <td>{store.complement}</td>
                  <td>{store.city}</td>
                  <td>{store.postalCode}</td>
                  <td>{store.country}</td>
                  <td>{store.url}</td>
                  <td>{store.gps}</td>
                  <td>
                    {store.products
                      ? store.products.map((val, j) => (
                          <span key={j}>
                            <Link to={`product/${val.id}`}>{val.id}</Link>
                            {j === store.products.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${store.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${store.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${store.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="lacachedartApp.store.home.notFound">No Stores found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ store }: IRootState) => ({
  storeList: store.entities,
  loading: store.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Store);
