package fr.afpa.service.impl;

import fr.afpa.service.CategoryService;
import fr.afpa.domain.Category;
import fr.afpa.repository.CategoryRepository;
import fr.afpa.service.dto.CategoryDTO;
import fr.afpa.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public CategoryDTO save(CategoryDTO categoryDTO) {
        log.debug("Request to save Category : {}", categoryDTO);
        Category category = categoryMapper.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryMapper.toDto(category);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryDTO> findAll() {
        log.debug("Request to get all Categories");
        return categoryRepository.findAllWithEagerRelationships().stream()
            .map(categoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    public Page<CategoryDTO> findAllWithEagerRelationships(Pageable pageable) {
        return categoryRepository.findAllWithEagerRelationships(pageable).map(categoryMapper::toDto);
    }
    

    /**
     * Get all main categories (ones which do not have parent category)
     * 
     * @return a list of categories without parent categories
     * 
     * @author Cecile
     */
	@Override
	@Transactional(readOnly = true)
	public List<CategoryDTO> getAllMainCategories() {
		log.debug("Request to get all main Categories");
		return this.categoryRepository.getAllMainCategories()
				.stream()
				.map(categoryMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<CategoryDTO> findByParent(CategoryDTO mainCategory) {
		Category category = categoryMapper.toEntity(mainCategory);
		return this.categoryRepository.findByParent(category)
			.stream()
			.map(categoryMapper::toDto)
			.collect(Collectors.toCollection(LinkedList::new));
	}

    @Override
    @Transactional(readOnly = true)
    public Optional<CategoryDTO> findOne(Long id) {
        log.debug("Request to get Category : {}", id);
        return categoryRepository.findOneWithEagerRelationships(id)
            .map(categoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Category : {}", id);
        categoryRepository.deleteById(id);
    }
}
