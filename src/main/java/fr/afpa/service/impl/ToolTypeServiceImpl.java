package fr.afpa.service.impl;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.domain.Category;
import fr.afpa.domain.ToolType;
import fr.afpa.repository.ToolTypeRepository;
import fr.afpa.service.ToolTypeService;
import fr.afpa.service.dto.CategoryDTO;
import fr.afpa.service.dto.ToolTypeDTO;
import fr.afpa.service.mapper.CategoryMapper;
import fr.afpa.service.mapper.ToolTypeMapper;

/**
 * Service Implementation for managing {@link ToolType}.
 */
@Service
@Transactional
public class ToolTypeServiceImpl implements ToolTypeService {

    private final Logger log = LoggerFactory.getLogger(ToolTypeServiceImpl.class);

    private final ToolTypeRepository toolTypeRepository;

    private final ToolTypeMapper toolTypeMapper;
    
    private final CategoryMapper categoryMapper;
    

    public ToolTypeServiceImpl(ToolTypeRepository toolTypeRepository, ToolTypeMapper toolTypeMapper, CategoryMapper categoryMapper) {
        this.toolTypeRepository = toolTypeRepository;
        this.toolTypeMapper = toolTypeMapper;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public ToolTypeDTO save(ToolTypeDTO toolTypeDTO) {
        log.debug("Request to save ToolType : {}", toolTypeDTO);
        ToolType toolType = toolTypeMapper.toEntity(toolTypeDTO);
        toolType = toolTypeRepository.save(toolType);
        return toolTypeMapper.toDto(toolType);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ToolTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ToolTypes");
        return toolTypeRepository.findAll(pageable)
            .map(toolTypeMapper::toDto);
    }
    
    
    /**
     * Get all ToolTypes depending of categories
     * 
     * @param Set CategoryDTO the Category Set
     * @return a List containing ToolTypeDTO
     * 
     * @author Cecile
     */
    @Override
    @Transactional(readOnly = true)
    public List<ToolTypeDTO> findAllByCategoriesIn(Set <CategoryDTO> categoriesDto) {
    	log.debug("Request to get all ToolTypes by Category");
    	
    	Set <Category> categories = new HashSet<>();
    	
    	for (CategoryDTO categDto : categoriesDto) {
    		categories.add(categoryMapper.toEntity(categDto));
    	}

        return toolTypeRepository.findAllByCategoriesIn(categories).stream()
            .map(toolTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ToolTypeDTO> findOne(Long id) {
        log.debug("Request to get ToolType : {}", id);
        return toolTypeRepository.findById(id)
            .map(toolTypeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ToolType : {}", id);
        toolTypeRepository.deleteById(id);
    }
}
