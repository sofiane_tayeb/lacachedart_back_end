package fr.afpa.service.impl;

import fr.afpa.service.ProductService;
import fr.afpa.domain.Product;
import fr.afpa.repository.ProductRepository;
import fr.afpa.service.dto.ProductDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;
import fr.afpa.service.mapper.ProductMapper;
import fr.afpa.service.mapper.UserOtherInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Product}.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final UserOtherInfoMapper userOtherInfoMapper;

    public ProductServiceImpl(
        ProductRepository productRepository,
        ProductMapper productMapper,
        UserOtherInfoMapper userOtherInfoMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.userOtherInfoMapper = userOtherInfoMapper;
    }

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        log.debug("Request to save Product : {}", productDTO);
        Product product = productMapper.toEntity(productDTO);
        product = productRepository.save(product);
        return productMapper.toDto(product);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Products");
        return productRepository.findAll(pageable)
            .map(productMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ProductDTO> findOne(Long id) {
        log.debug("Request to get Product : {}", id);
        return productRepository.findById(id)
            .map(productMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Product : {}", id);
        productRepository.deleteById(id);
    }

    @Override
    public Page<ProductDTO> retrieveProductsByUserOI(UserOtherInfoDTO userOI, Pageable pageable) {
        return productRepository
            .findByUserOtherInfo(pageable, userOtherInfoMapper.toEntity(userOI))
            .map(productMapper::toDto);
    }
}
