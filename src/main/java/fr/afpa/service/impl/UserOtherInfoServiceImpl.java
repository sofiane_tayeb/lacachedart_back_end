package fr.afpa.service.impl;

import fr.afpa.domain.User;
import fr.afpa.domain.UserOtherInfo;
import fr.afpa.repository.UserOtherInfoRepository;
import fr.afpa.service.UserOtherInfoService;
import fr.afpa.service.dto.UserDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;
import fr.afpa.service.mapper.UserMapper;
import fr.afpa.service.mapper.UserOtherInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing {@link UserOtherInfo}.
 */
@Service
@Transactional
public class UserOtherInfoServiceImpl implements UserOtherInfoService {

	private final Logger log = LoggerFactory.getLogger(UserOtherInfoServiceImpl.class);

	private final UserOtherInfoRepository userOtherInfoRepository;

	private final UserOtherInfoMapper userOtherInfoMapper;

    private final UserMapper userMapper;

    public UserOtherInfoServiceImpl(
        UserOtherInfoRepository userOtherInfoRepository,
        UserOtherInfoMapper userOtherInfoMapper,
        UserMapper userMapper)
    {
        this.userOtherInfoRepository = userOtherInfoRepository;
        this.userOtherInfoMapper = userOtherInfoMapper;
        this.userMapper = userMapper;
    }


	@Override
	public UserOtherInfoDTO save(UserOtherInfoDTO userOtherInfoDTO) {
		log.debug("Request to save UserOtherInfo : {}", userOtherInfoDTO);
		UserOtherInfo userOtherInfo = userOtherInfoMapper.toEntity(userOtherInfoDTO);
		userOtherInfo = userOtherInfoRepository.save(userOtherInfo);
		return userOtherInfoMapper.toDto(userOtherInfo);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<UserOtherInfoDTO> findAll(Pageable pageable) {
		log.debug("Request to get all UserOtherInfos");
		return userOtherInfoRepository.findAll(pageable).map(userOtherInfoMapper::toDto);
	}

    @Override
    @Transactional(readOnly = true)
    public Optional<UserOtherInfoDTO> findOne(Long id) {
        log.debug("Request to get UserOtherInfo : {}", id);
        return userOtherInfoRepository.findById(id)
            .map(userOtherInfoMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<UserOtherInfoDTO> findByUser(UserDTO userDto) {
    	log.debug("Request to get UserOtherInfo by User : {}", userDto);
    	User user = this.userMapper.userDTOToUser(userDto);
    	return this.userOtherInfoRepository.findByUser(user)
    			.map(userOtherInfoMapper::toDto);
    }

    @Override
    public void bindWithJhUser(Long jhUserId) {
        UserOtherInfoDTO bindJhUser = new UserOtherInfoDTO();
        bindJhUser.setUserId(jhUserId);
        bindJhUser.setPoints(0);
        bindJhUser.setBanned(false);
        ZonedDateTime registerTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        bindJhUser.setSubscriptionDate(registerTime);

        // save userOtherInfo bound to JHuser
        userOtherInfoRepository.save(userOtherInfoMapper.toEntity(bindJhUser));
    }

    @Override
    public UserOtherInfoDTO retrieveUserOIwithJhUser(UserDTO userPayload) {
        Optional<UserOtherInfoDTO> user = userOtherInfoRepository
            .findByUser(userMapper.userDTOToUser(userPayload))
            .map(userOtherInfoMapper::toDto);
    return user.get();
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserOtherInfo : {}", id);
        userOtherInfoRepository.deleteById(id);
    }
}
