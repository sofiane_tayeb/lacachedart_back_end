package fr.afpa.service;

import fr.afpa.domain.Category;
import fr.afpa.service.dto.CategoryDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.afpa.domain.Category}.
 */
public interface CategoryService {

    /**
     * Save a category.
     *
     * @param categoryDTO the entity to save.
     * @return the persisted entity.
     */
    CategoryDTO save(CategoryDTO categoryDTO);

    /**
     * Get all the categories.
     *
     * @return the list of entities.
     */
    List<CategoryDTO> findAll();

    /**
     * Get all the categories with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<CategoryDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get all main categories (ones which do not have parent category)
     * 
     * @return a list of categories without parent categories
     * 
     * @author Cecile
     */
    List<CategoryDTO> getAllMainCategories();

    /**
     * Get all subcategories of a main category
     * 
     * @return the list of subcategories of a main category
     * 
     * @author Cecile
     */    
    List<CategoryDTO> findByParent(CategoryDTO mainCategory);

    /**
     * Get the "id" category.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CategoryDTO> findOne(Long id);

    /**
     * Delete the "id" category.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
