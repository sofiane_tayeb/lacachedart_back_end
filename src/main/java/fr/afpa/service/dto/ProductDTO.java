package fr.afpa.service.dto;

import fr.afpa.domain.Photo;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link fr.afpa.domain.Product} entity.
 */
public class ProductDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    private Double manufacturerPrice;

    @NotNull
    private ZonedDateTime creationDate;

    @NotNull
    private String reference;

    @NotNull
    private Boolean active;


    private Long userOtherInfoId;

    private Long categoryId;

    private Long brandId;

    private Long toolTypeId;

    private Set<Photo> photos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getManufacturerPrice() {
        return manufacturerPrice;
    }

    public void setManufacturerPrice(Double manufacturerPrice) {
        this.manufacturerPrice = manufacturerPrice;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getUserOtherInfoId() {
        return userOtherInfoId;
    }

    public void setUserOtherInfoId(Long userOtherInfoId) {
        this.userOtherInfoId = userOtherInfoId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getToolTypeId() {
        return toolTypeId;
    }

    public void setToolTypeId(Long toolTypeId) {
        this.toolTypeId = toolTypeId;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDTO)) {
            return false;
        }

        return id != null && id.equals(((ProductDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", manufacturerPrice=" + getManufacturerPrice() +
            ", creationDate='" + getCreationDate() + "'" +
            ", reference='" + getReference() + "'" +
            ", active='" + isActive() + "'" +
            ", userOtherInfoId=" + getUserOtherInfoId() +
            ", categoryId=" + getCategoryId() +
            ", brandId=" + getBrandId() +
            ", toolTypeId=" + getToolTypeId() +
            "}";
    }
}
