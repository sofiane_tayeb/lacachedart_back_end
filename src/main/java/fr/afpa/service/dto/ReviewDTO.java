package fr.afpa.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.afpa.domain.Review} entity.
 */
public class ReviewDTO implements Serializable {
    
    private Long id;

    private String content;

    @NotNull
    private Integer note;

    @NotNull
    private Boolean active;


    private Long userOtherInfoId;

    private Long productId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getUserOtherInfoId() {
        return userOtherInfoId;
    }

    public void setUserOtherInfoId(Long userOtherInfoId) {
        this.userOtherInfoId = userOtherInfoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReviewDTO)) {
            return false;
        }

        return id != null && id.equals(((ReviewDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ReviewDTO{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", note=" + getNote() +
            ", active='" + isActive() + "'" +
            ", userOtherInfoId=" + getUserOtherInfoId() +
            ", productId=" + getProductId() +
            "}";
    }
}
