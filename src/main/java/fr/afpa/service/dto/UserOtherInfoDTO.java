package fr.afpa.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.afpa.domain.UserOtherInfo} entity.
 */
public class UserOtherInfoDTO implements Serializable {
    
    private Long id;

    private ZonedDateTime subscriptionDate;

    private Integer points;

    @NotNull
    private Boolean banned;


    private Long userId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(ZonedDateTime subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Boolean isBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserOtherInfoDTO)) {
            return false;
        }

        return id != null && id.equals(((UserOtherInfoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserOtherInfoDTO{" +
            "id=" + getId() +
            ", subscriptionDate='" + getSubscriptionDate() + "'" +
            ", points=" + getPoints() +
            ", banned='" + isBanned() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
