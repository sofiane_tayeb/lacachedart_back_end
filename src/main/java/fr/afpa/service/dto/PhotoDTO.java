package fr.afpa.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link fr.afpa.domain.Photo} entity.
 */
public class PhotoDTO implements Serializable {
    
    private Long id;

    private String link;

    @NotNull
    private Boolean active;


    private Long userOtherInfoId;

    private Long productId;

    private Long reviewId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getUserOtherInfoId() {
        return userOtherInfoId;
    }

    public void setUserOtherInfoId(Long userOtherInfoId) {
        this.userOtherInfoId = userOtherInfoId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoDTO)) {
            return false;
        }

        return id != null && id.equals(((PhotoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PhotoDTO{" +
            "id=" + getId() +
            ", link='" + getLink() + "'" +
            ", active='" + isActive() + "'" +
            ", userOtherInfoId=" + getUserOtherInfoId() +
            ", productId=" + getProductId() +
            ", reviewId=" + getReviewId() +
            "}";
    }
}
