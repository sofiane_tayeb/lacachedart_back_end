package fr.afpa.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link fr.afpa.domain.Category} entity.
 */
public class CategoryDTO implements Serializable {
    
    private Long id;

    private String name;


    private Long parentId;
    private Set<ProductDTO> products = new HashSet<>();
    private Set<ToolTypeDTO> toolTypes = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long categoryId) {
        this.parentId = categoryId;
    }

    public Set<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductDTO> products) {
        this.products = products;
    }

    public Set<ToolTypeDTO> getToolTypes() {
        return toolTypes;
    }

    public void setToolTypes(Set<ToolTypeDTO> toolTypes) {
        this.toolTypes = toolTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryDTO)) {
            return false;
        }

        return id != null && id.equals(((CategoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CategoryDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", parentId=" + getParentId() +
            ", products='" + getProducts() + "'" +
            ", toolTypes='" + getToolTypes() + "'" +
            "}";
    }
}
