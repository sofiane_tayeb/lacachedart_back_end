package fr.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.afpa.service.dto.CategoryDTO;
import fr.afpa.service.dto.ToolTypeDTO;

/**
 * Service Interface for managing {@link fr.afpa.domain.ToolType}.
 */
public interface ToolTypeService {

    /**
     * Save a toolType.
     *
     * @param toolTypeDTO the entity to save.
     * @return the persisted entity.
     */
    ToolTypeDTO save(ToolTypeDTO toolTypeDTO);

    /**
     * Get all the toolTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ToolTypeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" toolType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ToolTypeDTO> findOne(Long id);

    /**
     * Delete the "id" toolType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    /**
     * Get all ToolTypes depending of categories
     * 
     * @param Set CategoryDTO the Category Set
     * @return a List containing ToolTypeDTO
     * 
     * @author Cecile
     */
	List<ToolTypeDTO> findAllByCategoriesIn(Set <CategoryDTO> categoriesDto);
}
