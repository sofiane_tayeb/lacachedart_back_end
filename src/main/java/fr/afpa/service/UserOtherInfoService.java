package fr.afpa.service;

import fr.afpa.service.dto.UserDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link fr.afpa.domain.UserOtherInfo}.
 */
public interface UserOtherInfoService {

    /**
     * Save a userOtherInfo.
     *
     * @param userOtherInfoDTO the entity to save.
     * @return the persisted entity.
     */
    UserOtherInfoDTO save(UserOtherInfoDTO userOtherInfoDTO);

    /**
     * Get all the userOtherInfos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserOtherInfoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userOtherInfo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserOtherInfoDTO> findOne(Long id);

    /**
     * Find a UserOtherInfo by User
     *
     * @param user the UserDTO used to get the UserOtherInfo
     * @return UserOtherInfoDTO
     *
     * @author Cecile
     */
    Optional<UserOtherInfoDTO> findByUser(UserDTO user);


    /**
     * @author jerome
     *
     * Binds newly created JHipster User with a UserOtherInfo
     *
     * @param jhUserId
     */
    void bindWithJhUser (Long jhUserId);

    /**
     * @author jerome
     *
     * Retrieves a UserOtherInfo with a given JHipster User
     *
     * @param userPayload
     * @return
     */
    UserOtherInfoDTO retrieveUserOIwithJhUser(UserDTO userPayload);

    /**
     * Delete the "id" userOtherInfo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
