package fr.afpa.service.mapper;


import fr.afpa.domain.*;
import fr.afpa.service.dto.PhotoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Photo} and its DTO {@link PhotoDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserOtherInfoMapper.class, ProductMapper.class, ReviewMapper.class})
public interface PhotoMapper extends EntityMapper<PhotoDTO, Photo> {

    @Mapping(source = "userOtherInfo.id", target = "userOtherInfoId")
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "review.id", target = "reviewId")
    PhotoDTO toDto(Photo photo);

    @Mapping(source = "userOtherInfoId", target = "userOtherInfo")
    @Mapping(source = "productId", target = "product")
    @Mapping(source = "reviewId", target = "review")
    Photo toEntity(PhotoDTO photoDTO);

    default Photo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Photo photo = new Photo();
        photo.setId(id);
        return photo;
    }
}
