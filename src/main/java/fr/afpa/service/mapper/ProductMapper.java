package fr.afpa.service.mapper;


import fr.afpa.domain.*;
import fr.afpa.service.dto.ProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserOtherInfoMapper.class, CategoryMapper.class, BrandMapper.class, ToolTypeMapper.class, PhotoMapper.class})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(source = "userOtherInfo.id", target = "userOtherInfoId")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "brand.id", target = "brandId")
    @Mapping(source = "toolType.id", target = "toolTypeId")
    @Mapping(source = "photos", target = "photos")
    ProductDTO toDto(Product product);

    @Mapping(source = "userOtherInfoId", target = "userOtherInfo")
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "brandId", target = "brand")
    @Mapping(source = "toolTypeId", target = "toolType")
    @Mapping(target = "categories", ignore = true)
    @Mapping(target = "removeCategories", ignore = true)
    @Mapping(target = "stores", ignore = true)
    @Mapping(target = "removeStores", ignore = true)
    @Mapping(source = "photos", target = "photos")
    @Mapping(target = "removePhoto", ignore = true)
    Product toEntity(ProductDTO productDTO);

    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}
