package fr.afpa.service.mapper;


import fr.afpa.domain.*;
import fr.afpa.service.dto.ToolTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ToolType} and its DTO {@link ToolTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ToolTypeMapper extends EntityMapper<ToolTypeDTO, ToolType> {


    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    @Mapping(target = "categories", ignore = true)
    @Mapping(target = "removeCategories", ignore = true)
    ToolType toEntity(ToolTypeDTO toolTypeDTO);

    default ToolType fromId(Long id) {
        if (id == null) {
            return null;
        }
        ToolType toolType = new ToolType();
        toolType.setId(id);
        return toolType;
    }
}
