package fr.afpa.service.mapper;


import fr.afpa.domain.*;
import fr.afpa.service.dto.UserOtherInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserOtherInfo} and its DTO {@link UserOtherInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface UserOtherInfoMapper extends EntityMapper<UserOtherInfoDTO, UserOtherInfo> {

    @Mapping(source = "user.id", target = "userId")
    UserOtherInfoDTO toDto(UserOtherInfo userOtherInfo);

    @Mapping(source = "userId", target = "user")
    UserOtherInfo toEntity(UserOtherInfoDTO userOtherInfoDTO);

    default UserOtherInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserOtherInfo userOtherInfo = new UserOtherInfo();
        userOtherInfo.setId(id);
        return userOtherInfo;
    }
}
