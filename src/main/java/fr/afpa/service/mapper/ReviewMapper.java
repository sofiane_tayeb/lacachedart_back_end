package fr.afpa.service.mapper;


import fr.afpa.domain.*;
import fr.afpa.service.dto.ReviewDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Review} and its DTO {@link ReviewDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserOtherInfoMapper.class, ProductMapper.class})
public interface ReviewMapper extends EntityMapper<ReviewDTO, Review> {

    @Mapping(source = "userOtherInfo.id", target = "userOtherInfoId")
    @Mapping(source = "product.id", target = "productId")
    ReviewDTO toDto(Review review);

    @Mapping(source = "userOtherInfoId", target = "userOtherInfo")
    @Mapping(source = "productId", target = "product")
    @Mapping(target = "photos", ignore = true)
    @Mapping(target = "removePhoto", ignore = true)
    Review toEntity(ReviewDTO reviewDTO);

    default Review fromId(Long id) {
        if (id == null) {
            return null;
        }
        Review review = new Review();
        review.setId(id);
        return review;
    }
}
