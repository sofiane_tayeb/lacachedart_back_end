package fr.afpa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ToolType.
 */
@Entity
@Table(name = "tool_type")
public class ToolType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "toolType")
    private Set<Product> products = new HashSet<>();

    @ManyToMany(mappedBy = "toolTypes")
    @JsonIgnore
    private Set<Category> categories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ToolType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public ToolType products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public ToolType addProducts(Product product) {
        this.products.add(product);
        product.setToolType(this);
        return this;
    }

    public ToolType removeProducts(Product product) {
        this.products.remove(product);
        product.setToolType(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public ToolType categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public ToolType addCategories(Category category) {
        this.categories.add(category);
        category.getToolTypes().add(this);
        return this;
    }

    public ToolType removeCategories(Category category) {
        this.categories.remove(category);
        category.getToolTypes().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ToolType)) {
            return false;
        }
        return id != null && id.equals(((ToolType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ToolType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
