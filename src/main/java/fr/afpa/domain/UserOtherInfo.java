package fr.afpa.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A UserOtherInfo.
 */
@Entity
@Table(name = "user_other_info")
public class UserOtherInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "subscription_date")
    private ZonedDateTime subscriptionDate;

    @Column(name = "points")
    private Integer points;

    @NotNull
    @Column(name = "banned", nullable = false)
    private Boolean banned;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getSubscriptionDate() {
        return subscriptionDate;
    }

    public UserOtherInfo subscriptionDate(ZonedDateTime subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
        return this;
    }

    public void setSubscriptionDate(ZonedDateTime subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public Integer getPoints() {
        return points;
    }

    public UserOtherInfo points(Integer points) {
        this.points = points;
        return this;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Boolean isBanned() {
        return banned;
    }

    public UserOtherInfo banned(Boolean banned) {
        this.banned = banned;
        return this;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public User getUser() {
        return user;
    }

    public UserOtherInfo user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserOtherInfo)) {
            return false;
        }
        return id != null && id.equals(((UserOtherInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserOtherInfo{" +
            "id=" + getId() +
            ", subscriptionDate='" + getSubscriptionDate() + "'" +
            ", points=" + getPoints() +
            ", banned='" + isBanned() + "'" +
            "}";
    }
}
