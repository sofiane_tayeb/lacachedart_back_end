package fr.afpa.web.rest;

import fr.afpa.domain.Authority;
import fr.afpa.domain.User;
import fr.afpa.domain.UserOtherInfo;
import fr.afpa.security.jwt.JWTFilter;
import fr.afpa.security.jwt.TokenProvider;
import fr.afpa.service.UserOtherInfoService;
import fr.afpa.service.UserService;
import fr.afpa.service.dto.UserDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;
import fr.afpa.web.rest.vm.LoginVM;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    
    private final UserService userService;
    
    private final UserOtherInfoService userOtherInfoService;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder, 
    		UserService userService, UserOtherInfoService userOtherInfoService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userService = userService;
        this.userOtherInfoService = userOtherInfoService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

    	UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        

        Optional <User> user = this.userService.getUserWithAuthoritiesByLogin(loginVM.getUsername());
        Authority admin = new Authority();
        admin.setName("ROLE_ADMIN");
        
        if (user.isPresent() && !user.get().getAuthorities().contains(admin)) {
        	Optional <UserDTO> userDto = user.map(UserDTO::new);
        	Optional <UserOtherInfoDTO> userOtherInfo = this.userOtherInfoService.findByUser(userDto.get());
        	
        	if (userOtherInfo.isPresent() && userOtherInfo.get().isBanned()) {
        		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        	} else {
        		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        	}
        }
        
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);

        //return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
