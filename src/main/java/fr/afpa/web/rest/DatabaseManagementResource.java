package fr.afpa.web.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.security.AuthoritiesConstants;
import fr.afpa.service.BrandService;
import fr.afpa.service.CategoryService;
import fr.afpa.service.ProductService;
import fr.afpa.service.ToolTypeService;
import fr.afpa.service.UserOtherInfoService;
import fr.afpa.service.UserService;
import fr.afpa.service.dto.CategoryDTO;
import fr.afpa.service.dto.UserDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;


/**
 * Special class to add method to update database from JHipster API visual interface.
 * 
 * @author Cecile
 */
@RestController
@RequestMapping("/api")
public class DatabaseManagementResource {

	private static class DatabaseManagementResourceException extends RuntimeException {

		private DatabaseManagementResourceException(String message) {
			super(message);
		}
	}

	private final Logger log = LoggerFactory.getLogger(DatabaseManagementResource.class);

	private static final String ENTITY_NAME = "databaseManagement";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final CategoryService categoryService;
	private final ToolTypeService toolTypeService;
	private final UserService userService;
	private final UserOtherInfoService userOtherInfoService;

	public DatabaseManagementResource(CategoryService categoryService,
			ToolTypeService toolTypeService, UserService userService,
			UserOtherInfoService userOtherInfoService) {

		this.categoryService = categoryService;
		this.toolTypeService = toolTypeService;
		this.userService = userService;
		this.userOtherInfoService = userOtherInfoService;
	}

	/**
	 * Add foreign key to child categories
	 * 
	 * @return String - feedback about the success of the method
	 * 
	 * @author Cecile
	 */
	@GetMapping("/database-add-relation-category-child")
	public String addRelationToCategoryChild() {
		log.debug("REST request to add relation to Category children !");

		Optional<UserDTO> user = userService.getUserWithAuthorities().map(UserDTO::new);
		boolean isUpdated = false;

		if (user.get().getAuthorities().contains(AuthoritiesConstants.ADMIN)) {

			Optional<CategoryDTO> categoryCraie = this.categoryService.findOne((long) 5);	
			if (this.updateCategoryChildRelation(categoryCraie.get().getId(), (long)12, (long)1)) {
				isUpdated = true;
			}

			Optional<CategoryDTO> categoryBois = this.categoryService.findOne((long) 13);
			if (this.updateCategoryChildRelation(categoryBois.get().getId(), (long)16, (long)2)) {
				isUpdated = true;
			}

			Optional<CategoryDTO> categoryAcrylique = this.categoryService.findOne((long) 17);
			if (this.updateCategoryChildRelation(categoryAcrylique.get().getId(), (long)21, (long)3)) {
				isUpdated = true;
			}
			
			Optional<CategoryDTO> categoryArgile = this.categoryService.findOne((long) 22);
			if (this.updateCategoryChildRelation(categoryArgile.get().getId(), (long)27, (long)4)) {
				isUpdated = true;
			}

		} else {
			return "You do not have the permission to do this.";
		}

		return isUpdated ? "Congratulation, database updated !" : "Database already updated, no change was made.";
	}


	/**
	 * Add a foreign key to UserOtherInfo so 'artist' can be used without issue
	 * 
	 * @return String - feedback about the success of the method
	 * 
	 * @author Cecile
	 */
	@GetMapping("/database-add-relation-user-userotherinfo")
	public String addRelationBetweenUserAndUserOtherInfo() {
		log.debug("REST request to add relation between User 'artist' and UserOtherInfo !");

		Optional<UserDTO> user = userService.getUserWithAuthorities().map(UserDTO::new);
		boolean isUpdated = false;

		if (user.get().getAuthorities().contains(AuthoritiesConstants.ADMIN)) {
			
			UserOtherInfoDTO userOtherInfoDto = this.userOtherInfoService.findOne((long)1).get();
			if (userOtherInfoDto.getUserId() == null) {
				userOtherInfoDto.setUserId((long)5);
				this.userOtherInfoService.save(userOtherInfoDto);
				isUpdated = true;
			}

		} else {
			return "You do not have the permission to do this.";
		}
		return isUpdated ? "Congratulation, user 'artist' updated !" : "User 'artist' already updated, no change was made.";
	}
	
	

	/**
	 * Add relation between parent Cateegory and child Categories
	 * 
	 * @param startingId the child id from where to begin the relation
	 * @param maxId the child id from where to end the relation
	 * @param parentId the parent Category id to which children will be attributed
	 * @return boolean - true if updated
	 * 
	 * @author Cecile
	 */
	private boolean updateCategoryChildRelation(Long startingId, Long maxId, Long parentId) {

		Optional<CategoryDTO> startingCategory = this.categoryService.findOne(startingId);

		if (startingCategory.get().getParentId() == null) {
			for (Long i = startingId; i <= maxId; i++) {

				Optional<CategoryDTO> categoryDto = this.categoryService.findOne(i);

				if (categoryDto.isPresent()) {
					categoryDto.get().setParentId((long) parentId);
					this.categoryService.save(categoryDto.get());
				}
			}
			return true;
		}
		return false;
	}

}
