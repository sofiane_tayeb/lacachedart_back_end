package fr.afpa.web.rest;

import fr.afpa.service.*;
import fr.afpa.service.dto.ProductDTO;
import fr.afpa.service.dto.UserDTO;
import fr.afpa.service.dto.UserOtherInfoDTO;
import fr.afpa.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * REST controller for managing {@link fr.afpa.domain.Product}.
 */
@RestController
@RequestMapping("/api")
public class ProductResource {

	private static class ProductRessourceException extends RuntimeException {

		private ProductRessourceException(String message) {
			super(message);
		}
	}

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }

	private final Logger log = LoggerFactory.getLogger(ProductResource.class);

	private static final String ENTITY_NAME = "product";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final BrandService brandService;
	private final CategoryService categoryService;
	private final ProductService productService;
	private final ToolTypeService toolTypeService;
	private final UserService userService;
	private final UserOtherInfoService userOtherInfoService;

	public ProductResource(BrandService brandService, CategoryService categoryService, ProductService productService,
			ToolTypeService toolTypeService, UserService userService, UserOtherInfoService userOtherInfoService) {

		this.brandService = brandService;
		this.categoryService = categoryService;
		this.productService = productService;
		this.toolTypeService = toolTypeService;
		this.userService = userService;
		this.userOtherInfoService = userOtherInfoService;
	}


	/**
	 * {@code POST  /products} : Create a new product.
	 *
	 * @param productDTO the productDTO to create.
	 * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
	 *         body the new productDTO, or with status {@code 400 (Bad Request)} if
	 *         the product has already an ID.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PostMapping("/products")
	public ResponseEntity<ProductDTO> createProduct(@Valid @RequestBody ProductDTO productDTO)
			throws URISyntaxException {
		log.debug("REST request to save Product : {}", productDTO);
		if (productDTO.getId() != null) {
			throw new BadRequestAlertException("A new product cannot already have an ID", ENTITY_NAME, "idexists");
		}

		UserDTO userDTO = this.userService.getUserWithAuthorities().map(UserDTO::new)
				.orElseThrow(() -> new ProductRessourceException("User could not be found"));
		productDTO.setUserOtherInfoId(this.userOtherInfoService.findByUser(userDTO).get().getId());

		productDTO.setReference(this.generateReference(productDTO.getBrandId(),
				productDTO.getCategoryId(), productDTO.getToolTypeId()
		));


		if (productDTO.getManufacturerPrice() == null || productDTO.getManufacturerPrice().isNaN()) {
			productDTO.setManufacturerPrice(null);
		}

		ProductDTO result = productService.save(productDTO);
		return ResponseEntity
				.created(new URI("/api/products/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * Generate a reference with the aid of ids
	 *
	 * By using the Product foreign keys (ids from Brand, Category and ToolType) we
	 * get the corresponding objects from the database. From these object, we get
	 * their name and use a substring and a random number to create the reference.
	 *
	 * @param brandId    the id used to get the Brand in database
	 * @param categoryId the id used to get the Category in database
	 * @param toolTypeId the id used to get the ToolType in database
	 *
	 * @return the String reference
	 */
	public String generateReference(Long brandId, Long categoryId, Long toolTypeId) {

		String reference = new String();

		reference += this.categoryService.findOne(categoryId).get().getName().substring(0, 3);
		reference += this.toolTypeService.findOne(toolTypeId).get().getName().substring(0, 3);
		reference += this.brandService.findOne(brandId).get().getName().substring(0, 3);
		reference += String.valueOf(ThreadLocalRandom.current().nextInt(000000, 999999 + 1));

		return reference;
	}

    /**
     * @author jerome
     *
     * Retrieves a user with authorities sent by the token payload, userOtherInfo bound to it,
     * then get all the products belonging to this userOtherInfo.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of products in body.
     */
    @GetMapping("/products-belonging-to-user")
    public ResponseEntity<List<ProductDTO>> getAllProductsByUserOtherInfo(Pageable pageable) {
        log.debug("REST request to get a page of Products");

        UserDTO userPayload = userService
            .getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));

        UserOtherInfoDTO userOIRetrieved = userOtherInfoService
            .retrieveUserOIwithJhUser(userPayload);

        Page<ProductDTO> page = productService.retrieveProductsByUserOI(userOIRetrieved, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

	/**
	 * {@code PUT  /products} : Updates an existing product.
	 *
	 * @param productDTO the productDTO to update.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the updated productDTO, or with status {@code 400 (Bad Request)} if
	 *         the productDTO is not valid, or with status
	 *         {@code 500 (Internal Server Error)} if the productDTO couldn't be
	 *         updated.
	 * @throws URISyntaxException if the Location URI syntax is incorrect.
	 */
	@PutMapping("/products")
	public ResponseEntity<ProductDTO> updateProduct(@Valid @RequestBody ProductDTO productDTO)
			throws URISyntaxException {
		log.debug("REST request to update Product : {}", productDTO);
		if (productDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		ProductDTO result = productService.save(productDTO);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productDTO.getId().toString()))
				.body(result);
	}

	/**
	 * {@code GET  /products} : get all the products.
	 *
	 * @param pageable the pagination information.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
	 *         of products in body.
	 */
	@GetMapping("/products")
	public ResponseEntity<List<ProductDTO>> getAllProducts(Pageable pageable) {
		log.debug("REST request to get a page of Products");
		Page<ProductDTO> page = productService.findAll(pageable);
		HttpHeaders headers = PaginationUtil
				.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	/**
	 * {@code GET  /products/:id} : get the "id" product.
	 *
	 * @param id the id of the productDTO to retrieve.
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
	 *         the productDTO, or with status {@code 404 (Not Found)}.
	 */
	@GetMapping("/products/{id}")
	public ResponseEntity<ProductDTO> getProduct(@PathVariable Long id) {
		log.debug("REST request to get Product : {}", id);
		Optional<ProductDTO> productDTO = productService.findOne(id);
		return ResponseUtil.wrapOrNotFound(productDTO);
	}

	/**
	 * {@code DELETE  /products/:id} : delete the "id" product.
	 *
	 * @param id the id of the productDTO to delete.
	 * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	 */
	@DeleteMapping("/products/{id}")
	public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
		log.debug("REST request to delete Product : {}", id);
		productService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
				.build();
	}
}
