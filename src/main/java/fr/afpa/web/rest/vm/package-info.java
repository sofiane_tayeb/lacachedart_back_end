/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.afpa.web.rest.vm;
