package fr.afpa.repository;

import fr.afpa.domain.User;
import fr.afpa.domain.UserOtherInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the UserOtherInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserOtherInfoRepository extends JpaRepository<UserOtherInfo, Long> {


	Optional<UserOtherInfo> findByUser(User user);
}
