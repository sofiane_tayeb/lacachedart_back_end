package fr.afpa.repository;

import fr.afpa.domain.Product;

import fr.afpa.domain.UserOtherInfo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    /**
     * @author jerome
     *
     * Retrieves a pageable list of products bound to a UserotherInfo
     *
     * @param pageable
     * @param userOI
     * @return Pageable list of products created by current UserOtherInfo
     */
    Page<Product> findByUserOtherInfo(Pageable pageable, UserOtherInfo userOI);
}
