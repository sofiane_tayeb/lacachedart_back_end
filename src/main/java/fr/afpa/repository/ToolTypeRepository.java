package fr.afpa.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.afpa.domain.Category;
import fr.afpa.domain.ToolType;

/**
 * Spring Data  repository for the ToolType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ToolTypeRepository extends JpaRepository<ToolType, Long> {

	
	/**
     * Get all ToolTypes depending of categories
     * 
     * @param Set CategoryDTO the Category Set
     * @return a List containing ToolTypeDTO
     * 
     * @author Cecile
     */
	List<ToolType> findAllByCategoriesIn(Set <Category> categories);
}
