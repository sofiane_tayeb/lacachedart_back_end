package fr.afpa.repository;

import fr.afpa.domain.Category;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Category entity.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "select distinct category "
    		+ "from Category category "
    		+ "left join fetch category.products "
    		+ "left join fetch category.toolTypes",
        countQuery = "select count(distinct category) from Category category")
    Page<Category> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct category "
    		+ "from Category category "
    		+ "left join fetch category.products "
    		+ "left join fetch category.toolTypes")
    List<Category> findAllWithEagerRelationships();

    @Query("select category "
    		+ "from Category category "
    		+ "left join fetch category.products "
    		+ "left join fetch category.toolTypes "
    		+ "where category.id =:id")
    Optional<Category> findOneWithEagerRelationships(@Param("id") Long id);


    /**
     * Get all main categories (ones which do not have parent category)
     * 
     * @return a list of categories without parent categories
     * 
     * @author Cecile
     */
    @Query("select category "
    		+ "from Category category "
    		+ "where category.parent is null ")
    List<Category> getAllMainCategories();
    
    /**
     * Get all subcategories of a main category
     * 
     * @return the list of subcategories of a main category
     * 
     * @author Cecile
     */    
    List<Category> findByParent(Category mainCategory);
}
